/**
 * Created by Eoly on 2017/6/6.
 */

import com.alibaba.fastjson.JSON;
import com.cangshi.dao.UserMapper;
import com.cangshi.entity.User;
import com.cangshi.service.UserService;
import com.cangshi.utils.MD5Tools;
import com.cangshi.utils.UserUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(locations = {"classpath:spring-config.xml", "classpath:spring-mybatis.xml"})
public class TestUser {

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;

    @Test
    public void test_auto_build_user() {
        for (int i = 0; i < 10; i++) {
            User user = UserUtils.buildUser();
            try {
                userService.addUser(user);
            } catch (Exception e) {
                continue;
            }
        }
    }

    @Test
    public void can_search_by_condition() {
        User user = new User();
        user.setUserLogin("yZvrkX586_");
        user.setUserPass("123456");
        List<User> users = userMapper.selectUsers(0, 500, user);
        System.out.println("---->>>>>>>" + JSON.toJSONString(users));
    }

    @Test
    public void can_get_MD5(){
        System.out.println("--->>>"+ MD5Tools.MD5("123"));
    }
}
