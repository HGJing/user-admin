<%@page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><i class="fa fa-indent"></i>${param.navName}</h3>
        <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath}/users/index.html">Home</a></li>
            <li><i class="fa ${param.cssClass}"></i><a href="#">${param.name}</a></li>
        </ol>
    </div>
</div>