<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/6/5
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%@taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<script>
    app.controller('userList', function ($scope, $http) {
        $scope.delete = function (id) {
            Ewin.confirm({message: "确认要删除该用户？"}).on(function (e) {
                if (!e) {
                    return;
                }
                $http({
                    method: 'delete',
                    url: '${ pageContext.request.contextPath}/users/' + id
                }).then(function (res) {
                    alert(res.data.msg);
                    location.reload();
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                });
            });
        }
    });
</script>
<%--引入内容导航--%>
<jsp:include page="/WEB-INF/temp/commonMainNav.jsp">
    <jsp:param name="cssClass" value="fa-list-alt"/>
    <jsp:param name="navName" value="USER ADMIN"/>
    <jsp:param name="name" value="Users"/>
</jsp:include>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-indent red"></i><strong>用户列表</strong></h2>
        <div class="panel-actions">
            <a href="${pageContext.request.contextPath}/user/register.html" title="添加用户"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="${pageContext.request.contextPath}/users">
            <div class="input-group">
				                            <span class="input-group-btn">
				                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				                            </span>
                <input type="text" id="input1-group2" name="keywords" class="form-control"
                       placeholder="keywords" value="${pager.params.keywords}">
            </div>
        </form>
        <table class="table table-hover table-condensed" ng-controller="userList">
            <thead>
            <tr>
                <th>用户名</th>
                <th>登录名</th>
                <th>性别</th>
                <th>权限</th>
                <th>注册时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <c:forEach items="${pager.itemList}" var="user">
                <tr>
                    <td>${user.userDisplay}</td>
                    <td>${user.userLogin}</td>
                    <td>
                        <c:choose>
                            <c:when test="${user.userSex eq 0}">
                                <c:out value="男"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="女"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${user.userPower == 1}">
                                <span class="label label-warning">VIP</span>
                            </c:when>
                            <c:when test="${user.userPower == 2}">
                                <span class="label label-danger">ADMIN</span>
                            </c:when>
                            <c:when test="${user.userPower == 0}">
                                <span class="label label-default">NORMAL</span>
                            </c:when>
                            <c:otherwise>
                                <span class="label label-default">UNKNOWN</span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <fmt:formatDate value="${user.userSignTime}" type="both"/>
                    </td>

                    <td>
                        <shiro:hasPermission name="user:admin">
                            <a class="btn btn-info" title="编辑"
                               href="${ pageContext.request.contextPath}/user/editPage/${user.userId}">
                                <i class="fa fa-edit "></i>
                            </a>
                            <a class="btn btn-danger" title="删除该用户" href="javascript:;"
                               ng-click="delete(${user.userId})">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        </shiro:hasPermission>
                        <shiro:lacksPermission name="user:admin">
                            <span class="text">没有相应权限</span>
                            <c:if test="${user.userId eq userLogin.userId}">
                                <a class="btn btn-info" title="编辑"
                                   href="${ pageContext.request.contextPath}/user/editPage/${user.userId}">
                                    <i class="fa fa-edit "></i>
                                </a>
                            </c:if>
                        </shiro:lacksPermission>
                    </td>

                </tr>
            </c:forEach>
            <tfoot>
            <tr>
                <td colspan="6">
                    <jsp:include page="/WEB-INF/temp/common/pager-index.jsp"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
