<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="userBirthday" value="${user.userBirthday.time}"/>
<c:set var="userSex" value="${user.userSex}"/>
<c:if test="${empty user.userBirthday}">
    <c:set var="userBirthday" value="0"/>
</c:if>
<c:if test="${empty user.userSex}">
    <c:set var="userSex" value="-1"/>
</c:if>
<script>
    (function () {
        app.controller('edit', function ($scope, $http) {
            $scope.sexOption = [
                {
                    name: '请选择性别',
                    value: -1
                }, {
                    name: '男',
                    value: 0
                }, {
                    name: '女',
                    value: 1
                }
            ];
            $scope.statusOption = [
                {
                    name: '冻结',
                    value: -1
                }, {
                    name: '正常',
                    value: 0
                }
            ];
            $scope.powerOption = [
                {
                    name: '普通会员',
                    value: 0
                }, {
                    name: 'VIP会员',
                    value: 1
                }, {
                    name: '管理员',
                    value: 2
                }
            ];
            $scope.editPass = {};
            $scope.editStatusUser = {
                userStatus:${user.userStatus},
                userPower:${user.userPower}
            };
            $scope.userBase = {
                userLogin: '${user.userLogin}',
                userPass: '${user.userPass}',
                userDisplay: '${user.userDisplay}',
                userSex: ${userSex},
                userDetail: '${user.userDetail}',
                userBirthday: new Date(${userBirthday})
            };
            $scope.edit = function () {
                $http({
                    method: 'put',
                    data: $scope.userBase,
                    url: '${ pageContext.request.contextPath}/users/${user.userId}'
                }).then(function (res) {
                    alert(res.data.msg);
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                })
            };
            $scope.editPassword = function () {
                if ($scope.editPass.onePass != $scope.editPass.password) {
                    alert('两次输入的密码不同！');
                    return;
                }
                if (!$scope.editPass.password) {
                    alert('密码不能为空');
                    return;
                }
                $scope.editPass.userPass = $scope.editPass.password;
                $http({
                    method: 'put',
                    params: {type: 1},
                    data: $scope.editPass,
                    url: '${ pageContext.request.contextPath}/users/${user.userId}'
                }).then(function (res) {
                    alert(res.data.msg);
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                })
            };
            $scope.editStatus = function () {
                $scope.editPass.userPass = $scope.editPass.password;
                $http({
                    method: 'put',
                    params: {type: 2},
                    data: $scope.editStatusUser,
                    url: '${ pageContext.request.contextPath}/users/${user.userId}'
                }).then(function (res) {
                    alert(res.data.msg);
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                })
            }
        })
    })();
</script>
<%--引入内容导航--%>
<jsp:include page="/WEB-INF/temp/commonMainNav.jsp">
    <jsp:param name="cssClass" value="fa-list-alt"/>
    <jsp:param name="navName" value="USER ADMIN"/>
    <jsp:param name="name" value="Users-edit"/>
</jsp:include>
<div class="row" ng-controller="edit">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><i class="fa fa-indent red"></i><strong>用户信息编辑</strong></h2>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <form ng-submit="edit()">
                        <div class="form-group">
                            <label for="i_loginname">登录名:</label>
                            <div>
                                <input type="text" class="form-control" disabled ng-model="userBase.userLogin"
                                       id="i_loginname"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_display">用户昵称:</label>
                            <div>
                                <input type="text" class="form-control" ng-model="userBase.userDisplay" id="i_display"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_sex">用户性别:</label>
                            <div>
                                <select id="i_sex" class="form-control" ng-model="userBase.userSex"
                                        ng-options="x.value as x.name for x in sexOption"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_birthday">生日:</label>
                            <div>
                                <input id="i_birthday" ng-model="userBase.userBirthday" class="form-control"
                                       type="date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_detail">用户简介：</label>
                            <div>
                                <textarea id="i_detail" class="form-control" ng-model="userBase.userDetail"></textarea>
                                <br>
                                <button class="btn btn-default" type="submit">更新</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><i class="fa fa-indent red"></i><strong>修改用户密码</strong></h2>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <form ng-submit="editPassword()">
                        <div class="form-group">
                            <label for="i_pass">新密码：</label>
                            <div>
                                <input type="password" ng-model="editPass.onePass" class="form-control"
                                       placeholder="输入新密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_pass">再输入一次：</label>
                            <div>
                                <input type="password" ng-model="editPass.password" class="form-control" id="i_pass"
                                       placeholder="再输入一次">
                                <br>
                                <button class="btn btn-default" type="submit">修改</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><i class="fa fa-indent red"></i><strong>修改用户状态</strong></h2>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <form ng-submit="editStatus()">
                        <div class="form-group">
                            <label for="i_status">用户状态：</label>
                            <div>
                                <select id="i_status" class="form-control" ng-model="editStatusUser.userStatus"
                                        ng-options="x.value as x.name for x in statusOption"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="i_power">用户权限：</label>
                            <div>
                                <select id="i_power" class="form-control" ng-model="editStatusUser.userPower"
                                        ng-options="x.value as x.name for x in powerOption"></select>
                                <br>
                                <button class="btn btn-default" type="submit">修改</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

