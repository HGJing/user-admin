<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%--引入内容导航--%>
<jsp:include page="/WEB-INF/temp/commonMainNav.jsp">
    <jsp:param name="cssClass" value="fa-list-alt"/>
    <jsp:param name="navName" value="USER ADMIN"/>
    <jsp:param name="name" value="Users-add"/>
</jsp:include>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-indent red"></i><strong>添加用户</strong></h2>
    </div>
    <div class="panel-body">
        <sf:form cssClass="form-horizontal" method="post" acceptCharset="UTF-8" modelAttribute="user"
                 action="${ pageContext.request.contextPath}/user/register">
            <div class="form-group">
                <label for="i_loginname" class="control-label col-sm-2">登录名:</label>
                <div class="col-sm-10">
                    <sf:input path="userLogin" cssClass="form-control" id="i_loginname"/>
                    <p class="help-block">
                        <sf:errors path="userLogin"/>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="i_password" class="control-label col-sm-2">密码:</label>
                <div class="col-sm-10">
                    <sf:password path="userPass" cssClass="form-control" id="i_password"/>
                    <p class="help-block">
                        <sf:errors path="userPass"/>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="i_display" class="control-label col-sm-2">用户昵称:</label>
                <div class="col-sm-10">
                    <sf:input path="userDisplay" cssClass="form-control" id="i_display"/>
                    <p class="help-block">
                        <sf:errors path="userDisplay"/>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="i_sex" class="control-label col-sm-2">用户性别:</label>
                <div class="col-sm-10">
                    <sf:select path="userSex" id="i_sex" cssClass="form-control">
                        <sf:option value="-1">请选择性别</sf:option>
                        <sf:option value="0">男</sf:option>
                        <sf:option value="1">女</sf:option>
                    </sf:select>
                    <p class="help-block">
                        <sf:errors path="userSex"/>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="i_detail" class="control-label col-sm-2">用户简介：</label>
                <div class="col-sm-10">
                    <sf:textarea path="userDetail" id="i_detail" cssClass="form-control"/>
                    <p class="help-block">
                        <sf:errors path="userDetail"/>
                    </p>
                    <br>
                    <button type="submit" class="btn btn-default">添加</button>
                </div>

            </div>
        </sf:form>
    </div>
</div>