<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/6/5
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html;charset=utf-8" language="java" pageEncoding="utf-8" %>
<%@taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<script>
    app.controller('userList', function ($scope, $http) {
        $scope.letout = function (id) {
            Ewin.confirm({message: "确认要踢出该用户？"}).on(function (e) {
                if (!e) {
                    return;
                }
                $http({
                    method: 'post',
                    url: '${ pageContext.request.contextPath}/user/letout/' + id
                }).then(function (res) {
                    alert(res.data.msg);
                    location.reload();
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                });
            });
        }
    });
</script>
<%--引入内容导航--%>
<jsp:include page="/WEB-INF/temp/commonMainNav.jsp">
    <jsp:param name="cssClass" value="fa-list-alt"/>
    <jsp:param name="navName" value="USER ADMIN"/>
    <jsp:param name="name" value="Users online"/>
</jsp:include>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2><i class="fa fa-indent red"></i><strong>在线用户列表</strong></h2>
        <div class="panel-actions">
            <a href="${pageContext.request.contextPath}/user/register.html" title="添加用户"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="${pageContext.request.contextPath}/users/online.html">
            <div class="input-group">
				                            <span class="input-group-btn">
				                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
				                            </span>
                <input type="text" id="input1-group2" name="keywords" class="form-control"
                       placeholder="keywords" value="${pager.params.keywords}">
            </div>
        </form>
        <table class="table table-hover table-condensed" ng-controller="userList">
            <thead>
            <tr>
                <th>用户名</th>
                <th>登录名</th>
                <th>会话id</th>
                <th>性别</th>
                <th>权限</th>
                <th>注册时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <c:forEach items="${pager.itemList}" var="userMap">
                <tr>
                    <td>${userMap.value.userDisplay}</td>
                    <td>${userMap.value.userLogin}</td>
                    <td>${userMap.key}</td>
                    <td>
                        <c:choose>
                            <c:when test="${userMap.value.userSex eq 0}">
                                <c:out value="男"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="女"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${userMap.value.userPower == 1}">
                                <span class="label label-warning">VIP</span>
                            </c:when>
                            <c:when test="${userMap.value.userPower == 2}">
                                <span class="label label-danger">ADMIN</span>
                            </c:when>
                            <c:when test="${userMap.value.userPower == 0}">
                                <span class="label label-default">NORMAL</span>
                            </c:when>
                            <c:otherwise>
                                <span class="label label-default">UNKNOWN</span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <fmt:formatDate value="${userMap.value.userSignTime}" type="both"/>
                    </td>
                    <td>
                        <shiro:hasPermission name="user:admin">
                            <a class="btn btn-info" title="编辑"
                               href="${ pageContext.request.contextPath}/user/editPage/${userMap.value.userId}">
                                <i class="fa fa-edit "></i>
                            </a>
                            <a class="btn btn-danger" title="踢出该用户" href="javascript:;"
                               ng-click="letout('${userMap.key}')">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        </shiro:hasPermission>
                        <shiro:lacksPermission name="user:admin">
                            <span class="text">没有相应权限</span>
                        </shiro:lacksPermission>
                    </td>
                </tr>
            </c:forEach>
            <tfoot>
            <tr>
                <td colspan="7">
                    <jsp:include page="/WEB-INF/temp/common/pager-index.jsp"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
