<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%--引入内容导航--%>
<jsp:include page="/WEB-INF/temp/commonMainNav.jsp">
    <jsp:param name="navName" value="INDEX"/>
</jsp:include>
<div class="col-lg-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-font red"></i><strong>欢迎</strong></h2>
            <div class="panel-actions">
                <a href="javascript:;" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="well">
                欢迎使用该用户管理系统。
                <p class="text-right">——苍石</p>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2><i class="fa fa-bar-chart-o red"></i><strong>统计</strong></h2>
            <div class="panel-actions">
                <a href="javascript:;" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="sparkLineStats">

                <ul class="unstyled">

                    <li>
                        <div class="sparkLineStats3"></div>
                        用户总数:
                        <span class="number">${userNum}</span>
                    </li>
                    <li>
                        <div class="sparkLineStats3"></div>
                        在线用户:
                        <span class="number">${onlineNum}</span>
                    </li>
                    <li>
                        <div class="sparkLineStats4"></div>
                        页面访问量:
                        <span class="number">219</span>
                    </li>
                    <li>
                        <div class="sparkLineStats5"></div>
                        平均访问时长:
                        <span class="number">00:02:58</span>
                    </li>
                    <li>
                        <div class="sparkLineStats6"></div>
                        跳出率: <span class="number">59,83%</span>
                    </li>
                    <li>
                        <div class="sparkLineStats7"></div>
                        新用户访问率:
                        <span class="number">70,79%</span>
                    </li>
                    <li>
                        <div class="sparkLineStats8"></div>
                        回访率:
                        <span class="number">29,21%</span>
                    </li>
                </ul>
            </div><!-- End .sparkStats -->
        </div>
    </div>
</div>
