<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Import google fonts - Heading first/ text second -->
<!--<link rel='stylesheet' type='text/css'-->
<!--href='http://fonts.useso.com/css?family=Open+Sans:400,700|Droid+Sans:400,700'/>-->
<!--[if lt IE 9]>
<!--<link href="http://fonts.useso.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css"/>-->
<!--<link href="http://fonts.useso.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css"/>-->
<!--<link href="http://fonts.useso.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css"/>-->
<!--<link href="http://fonts.useso.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css"/>-->
<![endif]-->

<!-- Fav and touch icons -->
<link rel="shortcut icon" href="${ pageContext.request.contextPath}/resources/assets/ico/favicon.ico"
      type="image/x-icon"/>

<!-- Css files -->
<link href="${ pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/css/jquery.mmenu.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/css/climacons-font.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/plugins/xcharts/css/xcharts.min.css" rel=" stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/plugins/fullcalendar/css/fullcalendar.css"
      rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/plugins/morris/css/morris.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/plugins/jquery-ui/css/jquery-ui-1.10.4.min.css"
      rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/plugins/jvectormap/css/jquery-jvectormap-1.2.2.css"
      rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/css/style.min.css" rel="stylesheet">
<link href="${ pageContext.request.contextPath}/resources/assets/css/add-ons.min.css" rel="stylesheet">
<script src="${ pageContext.request.contextPath}/resources/angular.js"></script>
<script>
    var app = angular.module('app', []);
    app.config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'}
    })
</script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
<!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
<![endif]-->
