<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- start: JavaScript-->
<!--[if !IE]>-->

<script src="${ pageContext.request.contextPath}/resources/assets/js/jquery-2.1.1.min.js"></script>

<!--<![endif]-->

<!--[if IE]>

<script src="${ pageContext.request.contextPath}/resources/assets/js/jquery-1.11.1.min.js"></script>

<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
    window.jQuery || document.write("<script src='${ pageContext.request.contextPath}/resources/assets/js/jquery-2.1.1.min.js'>" + "<" + "/script>");
</script>

<!--<![endif]-->

<!--[if IE]>

<script type="text/javascript">
window.jQuery || document.write("<script src='${ pageContext.request.contextPath}/resources/assets/js/jquery-1.11.1.min.js'>" + "<" + "/script>");
</script>

<![endif]-->
<script src="${ pageContext.request.contextPath}/resources/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>


<!-- page scripts -->
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/jquery-ui/js/jquery-ui-1.10.4.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/moment/moment.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/fullcalendar/js/fullcalendar.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/flot/jquery.flot.spline.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/xcharts/js/xcharts.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/autosize/jquery.autosize.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/placeholder/jquery.placeholder.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/raphael/raphael.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/morris/js/morris.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/jvectormap/js/jquery-jvectormap-world-mill-en.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/jvectormap/js/gdp-data.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/gauge/gauge.min.js"></script>


<!-- theme scripts -->
<script src="${ pageContext.request.contextPath}/resources/assets/js/SmoothScroll.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/js/jquery.mmenu.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/js/core.min.js"></script>
<script src="${ pageContext.request.contextPath}/resources/assets/plugins/d3/d3.min.js"></script>
<script>
    (function ($) {

        window.Ewin = function () {
            var html = '<div id="[Id]" class="modal fade" role="dialog" aria-labelledby="modalLabel">' +
                '<div class="modal-dialog modal-sm">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                '<h4 class="modal-title" id="modalLabel">[Title]</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '<p>[Message]</p>' +
                '</div>' +
                '<div class="modal-footer">' +
                '<button type="button" class="btn btn-default cancel" data-dismiss="modal">[BtnCancel]</button>' +
                '<button type="button" class="btn btn-primary ok" data-dismiss="modal">[BtnOk]</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';


            var dialogdHtml = '<div id="[Id]" class="modal fade" role="dialog" aria-labelledby="modalLabel">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                '<h4 class="modal-title" id="modalLabel">[Title]</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            var reg = new RegExp("\\[([^\\[\\]]*?)\\]", 'igm');
            var generateId = function () {
                var date = new Date();
                return 'mdl' + date.valueOf();
            }
            var init = function (options) {
                options = $.extend({}, {
                    title: "操作提示",
                    message: "提示内容",
                    btnok: "确定",
                    btncl: "取消",
                    width: 200,
                    auto: false
                }, options || {});
                var modalId = generateId();
                var content = html.replace(reg, function (node, key) {
                    return {
                        Id: modalId,
                        Title: options.title,
                        Message: options.message,
                        BtnOk: options.btnok,
                        BtnCancel: options.btncl
                    }[key];
                });
                $('body').append(content);
                $('#' + modalId).modal({
                    width: options.width,
                    backdrop: 'static'
                });
                $('#' + modalId).on('hide.bs.modal', function (e) {
                    $('body').find('#' + modalId).remove();
                });
                return modalId;
            }

            return {
                alert: function (options) {
                    if (typeof options == 'string') {
                        options = {
                            message: options
                        };
                    }
                    var id = init(options);
                    var modal = $('#' + id);
                    modal.find('.ok').removeClass('btn-success').addClass('btn-primary');
                    modal.find('.cancel').hide();

                    return {
                        id: id,
                        on: function (callback) {
                            if (callback && callback instanceof Function) {
                                modal.find('.ok').click(function () {
                                    callback(true);
                                });
                            }
                        },
                        hide: function (callback) {
                            if (callback && callback instanceof Function) {
                                modal.on('hide.bs.modal', function (e) {
                                    callback(e);
                                });
                            }
                        }
                    };
                },
                confirm: function (options) {
                    var id = init(options);
                    var modal = $('#' + id);
                    modal.find('.ok').removeClass('btn-primary').addClass('btn-success');
                    modal.find('.cancel').show();
                    return {
                        id: id,
                        on: function (callback) {
                            if (callback && callback instanceof Function) {
                                modal.find('.ok').click(function () {
                                    callback(true);
                                });
                                modal.find('.cancel').click(function () {
                                    callback(false);
                                });
                            }
                        },
                        hide: function (callback) {
                            if (callback && callback instanceof Function) {
                                modal.on('hide.bs.modal', function (e) {
                                    callback(e);
                                });
                            }
                        }
                    };
                },
                dialog: function (options) {
                    options = $.extend({}, {
                        title: 'title',
                        url: '',
                        width: 800,
                        height: 550,
                        onReady: function () {
                        },
                        onShown: function (e) {
                        }
                    }, options || {});
                    var modalId = generateId();

                    var content = dialogdHtml.replace(reg, function (node, key) {
                        return {
                            Id: modalId,
                            Title: options.title
                        }[key];
                    });
                    $('body').append(content);
                    var target = $('#' + modalId);
                    target.find('.modal-body').load(options.url);
                    if (options.onReady())
                        options.onReady.call(target);
                    target.modal();
                    target.on('shown.bs.modal', function (e) {
                        if (options.onReady(e))
                            options.onReady.call(target, e);
                    });
                    target.on('hide.bs.modal', function (e) {
                        $('body').find(target).remove();
                    });
                }
            }
        }();
    })(jQuery);
</script>
<!-- inline scripts related to this page -->
<!--<script src="${ pageContext.request.contextPath}/resources/assets/js/pages/index.js"></script>-->

<!-- end: JavaScript-->