<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="sidebar ">
    <div class="sidebar-collapse">
        <div class="sidebar-header t-center">
            <h3 class="text">后台管理系统</h3>
        </div>
        <div class="sidebar-menu">
            <ul class="nav nav-sidebar">
                <li><a href="${pageContext.request.contextPath}/users/index.html"><i class="fa fa-laptop"></i><span
                        class="text">概况</span></a>
                </li>
                <%--<li>--%>
                <%--<a href="#"><i class="fa fa-file-text"></i><span class="text">用户管理</span> <span--%>
                <%--class="fa fa-angle-down pull-right"></span></a>--%>
                <%--<ul class="nav sub">--%>
                <%----%>
                <%--</ul>--%>
                <%--</li>--%>
                <li><a href="${pageContext.request.contextPath}/users"><i class="fa fa-user"></i><span
                        class="text">用户列表</span></a></li>
                <li><a href="${pageContext.request.contextPath}/users/online.html"><i
                        class="fa fa-magic"></i><span
                        class="text">在线用户管理</span></a></li>
                <li><a href="${pageContext.request.contextPath}/user/register.html"><i
                        class="fa fa-users"></i><span
                        class="text">注册/添加用户</span></a></li>
            </ul>
        </div>
    </div>
    <div class="sidebar-footer">

        <div class="sidebar-brand">
            user-admin
        </div>

    </div>
</div>
