<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${pager.params}" var="p">
    <c:set var="urlParam" value="${urlParam}&${p.key}=${p.value}" scope="page"/>
</c:forEach>
<jsp:useBean id='setting' class='java.util.HashMap' scope='page'>
    <c:set target='${setting}' property='index' value='center'/>
    <c:set target='${setting}' property='maxIndexPages' value='5'/>
    <c:forEach items="${param}" var="p">
        <c:set target='${setting}' property='${p.key}' value='${p.value}'/>
    </c:forEach>
</jsp:useBean>
<pg:pager
        url="${pager.url}"
        index="${setting.index}"
        maxPageItems="${pager.pageSize}"
        maxIndexPages="${setting.maxIndexPages}"
        items="${pager.itemSize}"
        export="pageOffset,currentPageNumber=pageNumber">

    <ul class="pagination center-block">
        <li class="disabled">
            <a>${pager.currentPage}/${pager.pageNumber}</a>
        </li>
        <pg:index>
            <pg:first>
                <li><a href="${pageUrl}${urlParam}">首页</a></li>
            </pg:first>
            <pg:prev>
                <li><a href="${pageUrl}${urlParam}">&laquo;</a></li>
            </pg:prev>
            <pg:pages>
                <c:choose>
                    <c:when test="${currentPageNumber eq pageNumber}">
                        <li class="active"><a>${pageNumber}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="${pageUrl}${urlParam}">${pageNumber}</a></li>
                    </c:otherwise>
                </c:choose>
            </pg:pages>
            <pg:next>
                <li><a href="${pageUrl}${urlParam}">&raquo;</a></li>
            </pg:next>
            <pg:last>
                <li><a href="${pageUrl}${urlParam}">尾页</a></li>
            </pg:last>
        </pg:index>
    </ul>
</pg:pager>