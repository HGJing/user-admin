<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <title>后台登录</title>
    <link href="${ pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="${ pageContext.request.contextPath}/resources/assets/css/climacons-font.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/login/login.css" type="text/css" rel="stylesheet">
    <script src="${ pageContext.request.contextPath}/resources/angular.js"></script>
</head>
<body>

<div class="login">
    <div class="message">用户管理系统登录</div>
    <div id="darkbannerwrap"></div>
    <div ng-controller="login">
        <form ng-submit="login()">
            <input name="action" value="login" type="hidden">
            <input name="username" ng-model="user.username" placeholder="用户名" required="" type="text">
            <hr class="hr15">
            <input name="password" ng-model="user.password" placeholder="密码" required="" type="password">
            <hr class="hr15">
            <button type="submit" class="submit" style="width:100%;">
                <i class="login-label"></i>
                <span class="login-span">登录</span>
            </button>
            <hr class="hr20">
            帮助 <a onClick="alert('请联系管理员')">忘记密码</a>
        </form>
    </div>
</div>
<script>
    (function () {
        var app = angular.module('app', []);
        var label = document.getElementsByClassName('login-label')[0];
        var loginSpan = document.getElementsByClassName('login-span')[0];
        app.controller('login', function ($scope, $http) {
            $scope.user = {};
            $scope.login = function () {
                loginSpan.innerHTML = '登录中...';
                label.className = 'login-label fa fa-spin fa-spinner';
                $http({
                    method: 'post',
                    params: $scope.user,
                    url: '${ pageContext.request.contextPath}/user/login'
                }).then(function (res) {
                    if (res.data.error == 0) {
                        loginSpan.innerHTML = '登录成功，正在跳转中...';
                        location.href = '${ pageContext.request.contextPath}/users/index.html'
                    } else {
                        alert(res.data.msg);
                        loginSpan.innerHTML = '登录';
                        label.className = 'login-label';
                    }
                }, function (res) {
                    if (res.data.msg) {
                        alert(res.data.msg);
                    } else {
                        alert('网络或设置错误');
                    }
                    loginSpan.innerHTML = '登录';
                    label.className = 'login-label';
                })
            }
        });
    })
    ();
</script>
</body>
</html>