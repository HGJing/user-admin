<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/6/7
  Time: 14:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>用户列表</title>
</head>
<body>
<table cellpadding="10" border="1" align="center">
    <%--<c:if test="${not empty keywords}">--%>
    <%--<pg:param name="keywords" value="${keywords}"/>--%>
    <%--</c:if>--%>
    <tr>
        <td colspan="4">
            <form action="user-list">
                <label>
                    <input type="text" name="keywords" value="${keywords}">
                    <input type="submit" value="查询">
                </label>
            </form>
        </td>
    </tr>
    <tr>
        <td>用户名</td>
        <td>登录名</td>
        <td>密码</td>
        <td>性别</td>
    </tr>
    <c:forEach items="${page.list}" var="user">
        <tr>
            <td>${user.userDisplay}</td>
            <td>${user.userLogin}</td>
            <td>${user.userPass}</td>
            <td>
                <c:choose>
                    <c:when test="${user.userSex eq 0}">
                        <c:out value="男"/>
                    </c:when>
                    <c:otherwise>
                        <c:out value="女"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="4">
            <c:if test="${!page.isFirstPage}">
                <a href="user-list?page=1&keywords=${keywords}">首页</a>
            </c:if>
            <c:if test="${page.hasPreviousPage}">
                <a href="user-list?page=${page.prePage}&keywords=${keywords}">上一页</a>
            </c:if>
            <c:forEach items="${page.navigatepageNums}" var="pageNum">
                <c:choose>
                    <c:when test="${page.pageNum eq pageNum}">
                        <font color="red">[${pageNum}]</font>
                    </c:when>
                    <%-- 当循环页码不是当前页码，则该页码可以导航 --%>
                    <c:otherwise>
                        <a href="user-list?page=${pageNum}&keywords=${keywords}">[${pageNum}]</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${page.hasNextPage}">
                <a href="user-list?page=${page.nextPage}&keywords=${keywords}">下一页</a>
            </c:if>
            <c:if test="${!page.isLastPage}">
                <a href="user-list?page=${page.pages}&keywords=${keywords}">尾页</a>
            </c:if>
        </td>
    </tr>
</table>
</body>
</html>
