<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/6/8
  Time: 9:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="app">
<head>
    <jsp:include page="/WEB-INF/temp/commonHeader.jsp"/>
    <title>用户列表</title>
</head>
<body>
<jsp:include page="/WEB-INF/temp/commonNavbar.jsp"/>
<div class="container-fluid content">
    <div class="row">
        <!-- start: Main Menu -->
        <jsp:include page="/WEB-INF/temp/commonSidebar.jsp"/>
        <!-- end: Main Menu -->

        <!-- start: Content -->
        <div class="main">
            <jsp:include page="/WEB-INF/temp/user/list.jsp"/>
        </div>
        <!-- end: Content -->
        <br><br><br>

    </div><!--/container-->


    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Warning Title</h4>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="clearfix"></div>
    <jsp:include page="/WEB-INF/temp/commonEndScript.jsp"/>
</div>
</body>
</html>
