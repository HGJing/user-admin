package com.cangshi.service;

import com.cangshi.entity.Pager;
import com.cangshi.entity.User;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Eoly on 2017/6/5.
 */
public interface UserService {

    User getUserByLoginName(String loginName);

    User login(String username, String password);

    Boolean logout();

    List<User> getUsersLimited(Integer offset, Integer userSize);

    Boolean deleteUserById(Integer id);

    User getUserById(Integer id);

    Boolean changePassword(Integer id, String newPassword);

    Boolean editUserBaseInfo(User user);

    Boolean editUserStatus(User user);

    Boolean addUser(User user);

    Integer getUserVolume();

    Integer getUserVolumeWithKeywords(String keywords);

    List<User> getUsersWithKeywords(Integer offset, Integer userSize, String keywords);

    PageInfo<User> getUsersByPageWithKeywords(Integer pageNumber, Integer pageSize, String keywords);

    Map<Serializable, User> getOnlineUsersWithKeywords(Integer offset, Integer userSize, String keywords);

    Integer getOnlineUserNum(String keywords);
}
