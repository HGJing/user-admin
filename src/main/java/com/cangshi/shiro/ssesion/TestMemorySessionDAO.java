package com.cangshi.shiro.ssesion;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by Eoly on 2017/3/31.
 */
public class TestMemorySessionDAO extends MemorySessionDAO {

    @Autowired
    private OnlineSessionManager onlineSessionManager;

    public TestMemorySessionDAO() {
        super();
    }

    @Override
    protected Serializable generateSessionId(Session session) {
        return super.generateSessionId(session);
    }

    @Override
    public void setSessionIdGenerator(SessionIdGenerator sessionIdGenerator) {
        super.setSessionIdGenerator(sessionIdGenerator);
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        this.storeSession(sessionId, session);
        return sessionId;
    }

    @Override
    protected Session storeSession(Serializable id, Session session) {
        return super.storeSession(id, session);
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        return super.doReadSession(sessionId);
    }

    @Override
    public void update(Session session) throws UnknownSessionException {
        super.update(session);
    }

    @Override
    public void delete(Session session) {
        super.delete(session);
        onlineSessionManager.removeSessionOnline(session.getId());
    }

    @Override
    public Collection<Session> getActiveSessions() {
        return super.getActiveSessions();
    }
}
