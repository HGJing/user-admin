package com.cangshi.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.cangshi.entity.JsonInfo;
import com.cangshi.exception.JsonException;
import com.cangshi.utils.HttpUtils;
import net.sf.json.util.WebUtils;
import org.apache.shiro.cache.ehcache.EhCache;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;


/**
 * Created by Eoly on 2017/3/31.
 */
public class KickoutSessionControlFilter extends AuthenticationFilter {

    private Integer maxSession;
    private boolean kickoutAfter;
    private DefaultWebSessionManager sessionManager;
    private String kickoutUrl;
    private EhCacheManager ehCacheManager;

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        Subject subject = getSubject(request, response);
        if (!subject.isAuthenticated() && !subject.isRemembered()) {
            //如果没有登录，直接进行之后的流程
            return true;
        }

        Session session = subject.getSession();
        String username = (String) subject.getPrincipal();
        Serializable sessionId = session.getId();
        EhCache ehCache = (EhCache) ehCacheManager.getCache("test");

        //TODO 同步控制
        Deque<Serializable> deque = (Deque<Serializable>) ehCache.get(username);
        if (deque == null) {
            deque = new LinkedList<Serializable>();
            ehCache.put(username, deque);
        }

        //如果队列里没有此sessionId，且用户没有被踢出；放入队列
        if (!deque.contains(sessionId) && session.getAttribute("kickout") == null) {
            deque.push(sessionId);
        }

        //如果队列里的sessionId数超出最大会话数，开始踢人
        while (deque.size() > maxSession) {
            Serializable kickoutSessionId = null;
            if (kickoutAfter) { //如果踢出后者
                kickoutSessionId = deque.removeFirst();
            } else { //否则踢出前者
                kickoutSessionId = deque.removeLast();
            }
            Session kickoutSession = null;
            try {
                kickoutSession =
                        sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
                //设置会话的kickout属性表示踢出了
                kickoutSession.setAttribute("kickout", true);
            } catch (UnknownSessionException e) {
                // do nothing
            }
        }

        //如果被踢出了，直接退出，重定向到踢出后的地址
        if (session.getAttribute("kickout") != null) {
            //会话被踢出了
            subject.logout();
            HttpUtils.sendJsonResultByCheckAjax(
                    new JsonInfo(450, "该用户在它处登录或被强制下线"),
                    request,
                    response
            );
        }

        return super.isAccessAllowed(request, response, mappedValue);
    }

    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        return true;
    }

    public Integer getMaxSession() {
        return maxSession;
    }

    public void setMaxSession(Integer maxSession) {
        this.maxSession = maxSession;
    }

    public boolean getKickoutAfter() {
        return kickoutAfter;
    }

    public void setKickoutAfter(boolean kickoutAfter) {
        this.kickoutAfter = kickoutAfter;
    }

    public DefaultWebSessionManager getSessionManager() {
        return sessionManager;
    }

    public void setSessionManager(DefaultWebSessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public String getKickoutUrl() {
        return kickoutUrl;
    }

    public void setKickoutUrl(String kickoutUrl) {
        this.kickoutUrl = kickoutUrl;
    }

    public boolean isKickoutAfter() {
        return kickoutAfter;
    }

    public EhCacheManager getEhCacheManager() {
        return ehCacheManager;
    }

    public void setEhCacheManager(EhCacheManager ehCacheManager) {
        this.ehCacheManager = ehCacheManager;
    }
}