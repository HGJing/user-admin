package com.cangshi.shiro.realm;

import com.alibaba.fastjson.JSON;
import com.cangshi.entity.User;
import com.cangshi.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Eoly on 2017/6/14.
 */
public class UserRealm extends AuthorizingRealm {

    @Autowired
    UserService userService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();

        User user = userService.getUserByLoginName(username);

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        Set<String> roles = new HashSet<String>();
        roles.add("user");
        authorizationInfo.setRoles(roles);
        Set<String> permissions = new HashSet<String>();
        switch (user.getUserPower()) {
            default:
            case 0:
                permissions.add("user:normal");
                break;
            case 1:
                permissions.add("user:normal,vip");
                break;
            case 2:
                permissions.add("user:*");
                break;
        }
        authorizationInfo.setStringPermissions(permissions);
        return authorizationInfo;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        UsernamePasswordToken login_token = (UsernamePasswordToken) token;


        User user = userService.getUserByLoginName(login_token.getUsername());

        Subject currentUser = SecurityUtils.getSubject();

        currentUser.getSession().setAttribute("userLogin", user);

        if (user == null) {
            throw new UnknownAccountException();//没找到帐号
        }

        if (user.getUserStatus() == -1) {
            throw new LockedAccountException(); //帐号锁定
        }

        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以自定义实现
        return new SimpleAuthenticationInfo(
                user.getUserLogin(), //用户名
                user.getUserPass(), //密码
                getName()  //realm name
        );
    }

    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

}
