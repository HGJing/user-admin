package com.cangshi.dao;

import com.cangshi.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> selectUsers(
            @Param("offset") Integer offset,
            @Param("userSize") Integer userSize,
            @Param("condition") User user
    );

    Integer selectCount(@Param("condition") User user);
}