package com.cangshi.interceptor;

import com.cangshi.entity.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Eoly on 2017/6/8.
 */
public class LoginCheckInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        User user = (User) request.getSession().getAttribute("userLogin");
        if (user == null || user.getUserPower() != 2) {
            String path = request.getContextPath();
            response.sendRedirect(path + "/user/login.html");
            return false;
        }
        return true;
    }
}
