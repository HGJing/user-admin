package com.cangshi.controller;

import com.alibaba.fastjson.JSONObject;
import com.cangshi.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Eoly on 2017/6/2.
 */
@Controller
@RequestMapping("/")
public class TestController {
    @Autowired
    UserService userService;

    @RequestMapping("index")
    public String test(Model model) {
        return "index";
    }

    @ResponseBody
    @RequiresPermissions("user:admin")
    @RequestMapping(value = "/get/userInfo", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    public String getUserInfo(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error",0);
        jsonObject.put("msg","获取成功");
        jsonObject.put("data",userService.getUsersLimited(null,null));
        return jsonObject.toJSONString();
    }

}
