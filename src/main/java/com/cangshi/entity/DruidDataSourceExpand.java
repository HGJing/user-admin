package com.cangshi.entity;

import com.alibaba.druid.pool.DruidDataSource;
import com.cangshi.utils.Base64;

/**
 * Created by Eoly on 2017/6/11.
 */
public class DruidDataSourceExpand extends DruidDataSource {

    public DruidDataSourceExpand() {
    }

    public DruidDataSourceExpand(boolean fairLock) {
        super(fairLock);
    }

    @Override
    public void setPassword(String password) {
        this.password = Base64.getFromBase64(password);
    }
}
