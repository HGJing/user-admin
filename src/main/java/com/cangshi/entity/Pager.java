package com.cangshi.entity;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Eoly on 2017/6/11.
 */
public class Pager<E> {
    private Integer pageNumber;
    private Integer currentPage;
    private Integer pageSize;
    private String url;
    private E itemList;
    private Integer itemSize;
    private Map<String, Object> params;

    public Pager() {
    }

    public Pager(E itemList, Integer pageSize, Integer itemSize, Integer offset) {
        this.setItemList(itemList);
        this.setItemSize(itemSize);
        this.setPageSize(pageSize);
        this.setCurrentPage(this.reckonCurrentPage(offset));
        this.setPageNumber(this.reckonPageNumber());
        this.params = new HashMap<String, Object>();
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public E getItemList() {
        return itemList;
    }

    public void setItemList(E itemList) {
        this.itemList = itemList;
    }

    public Integer getItemSize() {
        return itemSize;
    }

    public void setItemSize(Integer itemSize) {
        this.itemSize = itemSize;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Integer reckonCurrentPage(Integer offset) {
        return offset / getPageSize() + 1;
    }

    public Integer reckonPageNumber() {
        int itemSize = getItemSize();
        int pageSize = getPageSize();
        boolean isFullPage = itemSize % pageSize == 0;
        boolean isEmpty = itemSize == 0;
        int result = isEmpty ? 0 : itemSize / pageSize;
        if (!isFullPage || isEmpty) {
            result++;
        }
        return result;
    }

    public void addParam(String name, Object value) {
        this.params.put(name, value);
    }
}
