package com.cangshi.entity;

/**
 * Created by Eoly on 2017/6/14.
 */
public class PagerSetting {
    private Integer pageSize;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
